#!/usr/bin/python
import sys
import elevate
import python_hosts
import argparse
import tailscale_hosts
import tailscale_hosts.parser


def parse_hosts():
    return python_hosts.Hosts()


def merge_hosts(hosts, status):
    new_host_entries = []
    for k, v in status['Peer'].items():
        new_host_entries.append(
            python_hosts.HostsEntry(entry_type='ipv4', address=v['TailAddr'], names=[v['HostName']]))
    hosts.add(new_host_entries)
    return hosts


def remove_hosts(hosts, status):
    for k, v in status['Peer']:
        hosts.remove_all_matching(name=v['HostName'])
    return hosts


def write_hosts(hosts):
    try:
        hosts.write()
        return 0
    except python_hosts.exception.UnableToWriteHosts:
        pass

    elevate.elevate(graphical=False)

    try:
        hosts.write()
        return 0
    except python_hosts.exception.UnableToWriteHosts:
        return -1
        pass


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('command', help='Update hosts file with tailscale peer hostnames.',
                        choices=['update', 'remove'])
    if len(sys.argv[1:]) == 0:
        parser.print_help()
        parser.exit()
    args = parser.parse_args()

    tailscale_peers = tailscale_hosts.parser.TailscaleParser.status()
    hosts = parse_hosts()

    if not tailscale_peers or not hosts:
        return -1

    if 'update' == args.command:
        hosts = merge_hosts(hosts, tailscale_peers)
    elif 'remove' == args.command:
        hosts = remove_hosts(hosts, tailscale_peers)

    if not hosts:
        return -1

    write_hosts(hosts)
