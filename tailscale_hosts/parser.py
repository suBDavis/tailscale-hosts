import subprocess
import json


class TailscaleParser:
    """

    """

    @staticmethod
    def status():
        """

        :return:
        """
        try:
            res = subprocess.run(['tailscale', 'status', '--json'], capture_output=True)
        except FileNotFoundError:
            print('Failed to execute "tailscale status" command.')
            return None

        return json.loads(res.stdout)
